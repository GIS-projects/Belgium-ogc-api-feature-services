
# OGC API Features Services List Belgium
**A list of OGC API Features services with data for Belgium**

The entire content of this repository is stored in this README.md document. All editing should be done on this file. The only other file is the LICENSE file.

The names of the services are based on the *Title* and provided in each service.

More information about OGC API Features Services can be found on the [OGC](https://ogcapi.ogc.org/features/) website.

More detailed information about all listed services can be found on [features.michelstuyts.be](https://features.michelstuyts.be).

An xml file to import all these services into [QGIS](https://qgis.org) can be downloaded from https://features.michelstuyts.be/qgis.php?lang=en.

## Flanders

* [OGC API Features Adressen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ad/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Adressen (geharmoniseerde data).

* [OGC API Features Adressen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ad/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Adressen (geharmoniseerde data).

* [OGC API Features Adressenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Adressenregister/ogc/features): De Adressenregister-OGC API Features laat toe de objecten in het Adressenregister geometrisch te bevragen. Het Adressenregister verzamelt informatie over alle adressen op het Vlaamse grondgebied in één register. Het biedt een unieke en stabiele identificatie van adressen en beschrijft hun levensloop.

* [OGC API Features Adressenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Adressenregister/ogc/features/v1): De Adressenregister-OGC API Features laat toe de objecten in het Adressenregister geometrisch te bevragen. Het Adressenregister verzamelt informatie over alle adressen op het Vlaamse grondgebied in één register. Het biedt een unieke en stabiele identificatie van adressen en beschrijft hun levensloop.

* [OGC API Features Agglomeraties Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-Agglomerations/ogc/features): Directe downloadservice voor de Agglomeraties Richtlijn Stedelijk afvalwater.

* [OGC API Features Agglomeraties Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-Agglomerations/ogc/features/v1): Directe downloadservice voor de Agglomeraties Richtlijn Stedelijk afvalwater.

* [OGC API Features Bebossing op de Ferrariskaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosFerraris/ogc/features): Directe downloadservice voor historische bebossing zoals opgenomen op de Ferrariskaarten.

* [OGC API Features Bebossing op de Ferrariskaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosFerraris/ogc/features/v1): Directe downloadservice voor historische bebossing zoals opgenomen op de Ferrariskaarten.

* [OGC API Features Bebossing op de topografische kaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosTopokaart/ogc/features): Directe downloadservice voor de bebossing zoals opgenomen op de Topografische kaarten van 1910-1940.

* [OGC API Features Bebossing op de topografische kaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosTopokaart/ogc/features/v1): Directe downloadservice voor de bebossing zoals opgenomen op de Topografische kaarten van 1910-1940.

* [OGC API Features Bebossing op de Vandermaelenkaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosVandermaelen/ogc/features): Directe downloadservice voor de bebossing op de Vandermaelenkaarten.

* [OGC API Features Bebossing op de Vandermaelenkaarten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BosVandermaelen/ogc/features/v1): Directe downloadservice voor de bebossing op de Vandermaelenkaarten.

* [OGC API Features Bedrijventerreinen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bedrijventerreinen/ogc/features): Directe downloadservice voor de Bedrijventerreinen.

* [OGC API Features Bedrijventerreinen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bedrijventerreinen/ogc/features/v1): Directe downloadservice voor de Bedrijventerreinen.

* [OGC API Features Beheergebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BehGeb/ogc/features): Directe overdrachtdienst voor de verschillende typen Beheergebieden.

* [OGC API Features Beheergebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BehGeb/ogc/features/v1): Directe overdrachtdienst voor de verschillende typen Beheergebieden.

* [OGC API Features Beschermde gebieden ifv winning drinkwater uit oppervlakte- en grondwater Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-ProtectedArea/ogc/features): Directe downloadservice voor de Beschermde gebieden ifv winning drinkwater uit oppervlakte- en grondwater in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Beschermde gebieden ifv winning drinkwater uit oppervlakte- en grondwater Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-ProtectedArea/ogc/features/v1): Directe downloadservice voor de Beschermde gebieden ifv winning drinkwater uit oppervlakte- en grondwater in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Biologische Waarderingskaart en Natura 2000 Habitatkaart - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BWK/ogc/features): Directe downloadservice voor de Biologische Waarderingskaart en Natura 2000 Habitatkaart.

* [OGC API Features Biologische Waarderingskaart en Natura 2000 Habitatkaart - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/BWK/ogc/features/v1): Directe downloadservice voor de Biologische Waarderingskaart en Natura 2000 Habitatkaart.

* [OGC API Features Bosleeftijd - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bosleeftijd/ogc/features): Directe downloadservice voor de kaart met de bosleeftijd.

* [OGC API Features Bosleeftijd - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bosleeftijd/ogc/features/v1): Directe downloadservice voor de kaart met de bosleeftijd.

* [OGC API Features Bosreferentielaag 2000 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bosref/ogc/features): Directe downloadservice voor de boskartering 2000.

* [OGC API Features Bosreferentielaag 2000 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Bosref/ogc/features/v1): Directe downloadservice voor de boskartering 2000.

* [OGC API Features Brownfieldconvenanten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Brownfieldconvenanten/ogc/features): Directe downloadservice voor Brownfieldconvenanten.

* [OGC API Features Brownfieldconvenanten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Brownfieldconvenanten/ogc/features/v1): Directe downloadservice voor Brownfieldconvenanten.

* [OGC API Features EU Register van industriële sites - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/EUReg-EPRTR/ogc/features): Directe downloadservice voor de industriële activiteiten opgenomen in het EU register van industriële sites.

* [OGC API Features EU Register van industriële sites - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/EUReg-EPRTR/ogc/features/v1): Directe downloadservice voor de industriële activiteiten opgenomen in het EU register van industriële sites.

* [OGC API Features Gebieden met recht van voorkoop - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RVV/ogc/features): Directe downloadservice voor de gebieden met recht van voorkoop.

* [OGC API Features Gebieden met recht van voorkoop - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RVV/ogc/features/v1): Directe downloadservice voor de gebieden met recht van voorkoop.

* [OGC API Features Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/am/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema 'Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden' (geharmoniseerde data).

* [OGC API Features Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/am/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema 'Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden' (geharmoniseerde data).

* [OGC API Features Gebiedstypes nitraat mestdecreet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Gebnit/ogc/features): Directe downloadservice voor de gebiedstypes nitraat mestdecreet.

* [OGC API Features Gebiedstypes nitraat mestdecreet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Gebnit/ogc/features/v1): Directe downloadservice voor de gebiedstypes nitraat mestdecreet.

* [OGC API Features Gebouwen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/bu-core2d/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Gebouwen (geharmoniseerde data).

* [OGC API Features Gebouwen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/bu-core2d/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Gebouwen (geharmoniseerde data).

* [OGC API Features Gebouwen Vlaamse en Lokale Overheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GebouwenVLO/ogc/features): Directe downloadservice voor de gebouwen in eigendom van en/of in gebruik door de Vlaamse en Lokale Overheden.

* [OGC API Features Gebouwen Vlaamse en Lokale Overheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GebouwenVLO/ogc/features/v1): Directe downloadservice voor de gebouwen in eigendom van en/of in gebruik door de Vlaamse en Lokale Overheden.

* [OGC API Features Gebouwenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Gebouwenregister/ogc/features): De Gebouwenregister-OGC API Features laat toe de objecten in het Gebouwenregister geometrisch te bevragen. Het Gebouwenregister verzamelt informatie over alle gebouwen op het Vlaamse grondgebied in één register. Het biedt een unieke en stabiele identificatie van gebouwen en gebouweenheden (zoals appartementen en winkels) en beschrijft hun levensloop. De gebouwen betrekken hun grootschalige geometrie uit de Basiskaart Vlaanderen (GRB). Het Gebouwenregister wordt de authentieke (= als meest kwalitatief erkende) gegevensbron voor gebouwen in het Vlaamse Gewest en zal de koppeling en uitwisseling van gebouwgerelateerde informatie vereenvoudigen.

* [OGC API Features Gebouwenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Gebouwenregister/ogc/features/v1): De Gebouwenregister-OGC API Features laat toe de objecten in het Gebouwenregister geometrisch te bevragen. Het Gebouwenregister verzamelt informatie over alle gebouwen op het Vlaamse grondgebied in één register. Het biedt een unieke en stabiele identificatie van gebouwen en gebouweenheden (zoals appartementen en winkels) en beschrijft hun levensloop. De gebouwen betrekken hun grootschalige geometrie uit de Basiskaart Vlaanderen (GRB). Het Gebouwenregister wordt de authentieke (= als meest kwalitatief erkende) gegevensbron voor gebouwen in het Vlaamse Gewest en zal de koppeling en uitwisseling van gebouwgerelateerde informatie vereenvoudigen.

* [OGC API Features GIPOD - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GIPOD/ogc/features): GIPOD innames en geplande mobiliteitshinder.

* [OGC API Features GIPOD - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GIPOD/ogc/features/v1): GIPOD innames en geplande mobiliteitshinder.

* [OGC API Features GRB - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GRB/ogc/features): Via de OGC API Features GRB kan je de vectordata van de verschillende objecten uit het Grootschalig Referentiebestand (GRB) opvragen. De OGC API Features GRB bevat alle GRB-gegevens gebaseerd op het GRBgis product. De gebruiker kan selecteren welke GRB-gegevens opgevraagd worden. Voor een gedetailleerde databeschrijving van het GRB raadpleegt u best het GRB-objectenhandboek via https://overheid.vlaanderen.be/producten/grb/objectcatalogus/entiteiten.

* [OGC API Features GRB - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/GRB/ogc/features/v1): Via de OGC API Features GRB kan je de vectordata van de verschillende objecten uit het Grootschalig Referentiebestand (GRB) opvragen. De OGC API Features GRB bevat alle GRB-gegevens gebaseerd op het GRBgis product. De gebruiker kan selecteren welke GRB-gegevens opgevraagd worden. Voor een gedetailleerde databeschrijving van het GRB raadpleegt u best het GRB-objectenhandboek via https://overheid.vlaanderen.be/producten/grb/objectcatalogus/entiteiten.

* [OGC API Features GRB - Administratieve percelen fiscaal - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Adpf/ogc/features): Directe downloadservice voor de fiscale toestand op 1 januari van de GRB administratieve percelen.

* [OGC API Features GRB - Administratieve percelen fiscaal - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Adpf/ogc/features/v1): Directe downloadservice voor de fiscale toestand op 1 januari van de GRB administratieve percelen.

* [OGC API Features Grenzen van Polders - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Polders/ogc/features): Directe downloadservice voor de grenzen van polders.

* [OGC API Features Grenzen van Polders - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Polders/ogc/features/v1): Directe downloadservice voor de grenzen van polders.

* [OGC API Features Grenzen van wateringen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wateringen/ogc/features): Directe downloadservice voor de grenzen van wateringen.

* [OGC API Features Grenzen van wateringen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wateringen/ogc/features/v1): Directe downloadservice voor de grenzen van wateringen.

* [OGC API Features Haltes De Lijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Haltes/ogc/features): Directe downloadservice voor de haltes van de vervoersmaatschappij De Lijn.

* [OGC API Features Haltes De Lijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Haltes/ogc/features/v1): Directe downloadservice voor de haltes van de vervoersmaatschappij De Lijn.

* [OGC API Features historisch landgebruik - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/HistLandgebruik/ogc/features/v1): Directe downloadservice voor dvoor het historisch landgebruik.

* [OGC API Features Hydrografie - Fysieke wateren - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/hy-p/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Hydrografie - Fysieke wateren (geharmoniseerde data).

* [OGC API Features Hydrografie - Fysieke wateren - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/hy-p/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Hydrografie - Fysieke wateren (geharmoniseerde data).

* [OGC API Features Hydrografie - Netwerk - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/hy-n/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Hydrografie - Netwerk (geharmoniseerde data).

* [OGC API Features Hydrografie - Netwerk - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/hy-n/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Hydrografie - Netwerk (geharmoniseerde data).

* [OGC API Features Interessante Plaatsen (POI) - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/POI/ogc/features): Directe downloadservice voor Points Of Interest (POI).

* [OGC API Features Interessante Plaatsen (POI) - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/POI/ogc/features/v1): Directe downloadservice voor Points Of Interest (POI).

* [OGC API Features Jachtterreinen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Jacht/ogc/features): Directe downloadservice voor de jachtterreinen.

* [OGC API Features Jachtterreinen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Jacht/ogc/features/v1): Directe downloadservice voor de jachtterreinen.

* [OGC API Features kwetsbare gebieden Nitraatrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NID-NVZ/ogc/features): Directe downloadservice voor de kwetsbare gebieden zoals gerapporteerd onder de Nitraatrichtlijn aan de Europese Commissie.

* [OGC API Features kwetsbare gebieden Nitraatrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NID-NVZ/ogc/features/v1): Directe downloadservice voor de kwetsbare gebieden zoals gerapporteerd onder de Nitraatrichtlijn aan de Europese Commissie.

* [OGC API Features Kwetsbare gebieden Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-SensitiveAreas/ogc/features): Directe downloadservice voor de Kwetsbare gebieden Richtlijn Stedelijk afvalwater.

* [OGC API Features Kwetsbare gebieden Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-SensitiveAreas/ogc/features/v1): Directe downloadservice voor de Kwetsbare gebieden Richtlijn Stedelijk afvalwater.

* [OGC API Features Landbouwgebruikspercelen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landbgebrperc/ogc/features): Directe downloadservice voor de Landbouwgebruikspercelen.

* [OGC API Features Landbouwgebruikspercelen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landbgebrperc/ogc/features/v1): Directe downloadservice voor de Landbouwgebruikspercelen.

* [OGC API Features Landbouwstreken België - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landbouwstreken/ogc/features): Directe downloadservice voor de Landbouwstreken van België.

* [OGC API Features Landbouwstreken België - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landbouwstreken/ogc/features/v1): Directe downloadservice voor de Landbouwstreken van België.

* [OGC API Features Landgebruik - Bestaand landgebruik - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/elu/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Landgebruik - Bestaand landgebruik (geharmoniseerde data).

* [OGC API Features Landgebruik - Bestaand landgebruik - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/elu/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Landgebruik - Bestaand landgebruik (geharmoniseerde data).

* [OGC API Features Landinrichting - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landinrichting/ogc/features): Directe downloadservice voor Landinrichting.

* [OGC API Features Landinrichting - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Landinrichting/ogc/features/v1): Directe downloadservice voor Landinrichting.

* [OGC API Features Lozingspunten Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-DischargePoints/ogc/features): Directe downloadservice voor de Lozingspunten Richtlijn Stedelijk afvalwater.

* [OGC API Features Lozingspunten Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-DischargePoints/ogc/features/v1): Directe downloadservice voor de Lozingspunten Richtlijn Stedelijk afvalwater.

* [OGC API Features Meetnet Afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetnetAfvalwater/ogc/features): Directe downloadservice voor het Meetnet Afvalwater.

* [OGC API Features Meetnet Afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetnetAfvalwater/ogc/features/v1): Directe downloadservice voor het Meetnet Afvalwater.

* [OGC API Features Meetnet Riooloverstorten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetnetRiooloverstorten/ogc/features): Directe downloadservice voor het Meetnet Riooloverstorten.

* [OGC API Features Meetnet Riooloverstorten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetnetRiooloverstorten/ogc/features/v1): Directe downloadservice voor het Meetnet Riooloverstorten.

* [OGC API Features Meetplaatsen Oppervlaktewaterkwaliteit - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetplOppervlwaterkwal/ogc/features): Directe downloadservice voor de Meetplaatsen Oppervlaktewaterkwaliteit.

* [OGC API Features Meetplaatsen Oppervlaktewaterkwaliteit - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/MeetplOppervlwaterkwal/ogc/features/v1): Directe downloadservice voor de Meetplaatsen Oppervlaktewaterkwaliteit.

* [OGC API Features Meetpunten in oppervlakte- en grondwaterlichamen Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-MonitoringSite/ogc/features): Directe downloadservice voor de Meetpunten in oppervlakte- en grondwaterlichamen in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Meetpunten in oppervlakte- en grondwaterlichamen Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-MonitoringSite/ogc/features/v1): Directe downloadservice voor de Meetpunten in oppervlakte- en grondwaterlichamen in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Milieubewakingsvoorzieningen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ef/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Milieubewakingsvoorzieningen (geharmoniseerde data).

* [OGC API Features Milieubewakingsvoorzieningen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ef/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Milieubewakingsvoorzieningen (geharmoniseerde data).

* [OGC API Features Natuurinrichtingsprojecten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Natuurinrichting/ogc/features): Directe downloadservice voor de Natuurinrichtingsprojecten.

* [OGC API Features Natuurinrichtingsprojecten - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Natuurinrichting/ogc/features/v1): Directe downloadservice voor de Natuurinrichtingsprojecten.

* [OGC API Features Nutsdiensten en overheidsdiensten - Inrichtingen milieubeheer - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/us-emf/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Nutsdiensten en overheidsdiensten - Inrichtingen milieubeheer (geharmoniseerde data).

* [OGC API Features Nutsdiensten en overheidsdiensten - Nutsvoorzieningennet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/us-net-common/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Nutsdiensten en overheidsdiensten - Nutsvoorzieningennet (geharmoniseerde data).

* [OGC API Features Nutsdiensten en overheidsdiensten - Rioleringsnet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/us-net-sw/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex III Thema Nutsdiensten en overheidsdiensten - Rioleringsnet (geharmoniseerde data).

* [OGC API Features oppervlakte meetpunten Nitraatrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NID-SWMonitoringStations/ogc/features): Directe downloadservice voor de Oppervlakte meetpunten zoals gerapporteerd onder de nitraatrichtlijn aan de Europese Commissie.

* [OGC API Features oppervlakte meetpunten Nitraatrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NID-SWMonitoringStations/ogc/features/v1): Directe downloadservice voor de Oppervlakte meetpunten zoals gerapporteerd onder de nitraatrichtlijn aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Oppervlaktewaterlichamen/ogc/features): Directe downloadservice voor de geografische indeling van oppervlaktewaterlichamen.

* [OGC API Features Oppervlaktewaterlichamen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Oppervlaktewaterlichamen/ogc/features/v1): Directe downloadservice voor de geografische indeling van oppervlaktewaterlichamen.

* [OGC API Features Oppervlaktewaterlichamen (centerlines) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyCenterline/ogc/features): Directe downloadservice voor de Oppervlaktewaterlichamen (centerlines) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen (centerlines) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyCenterline/ogc/features/v1): Directe downloadservice voor de Oppervlaktewaterlichamen (centerlines) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen (lijnen) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyLine/ogc/features): Directe downloadservice voor de Oppervlaktewaterlichamen (lijnen) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen (lijnen) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyLine/ogc/features/v1): Directe downloadservice voor de Oppervlaktewaterlichamen (lijnen) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen (polygonen) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBody/ogc/features): Directe downloadservice voor de Oppervlaktewaterlichamen (polygonen) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Oppervlaktewaterlichamen (polygonen) Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBody/ogc/features/v1): Directe downloadservice voor de Oppervlaktewaterlichamen (polygonen) in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Overstromingsgebieden en oeverzones - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/OGOZ/ogc/features): Directe downloadservice voor de Overstromingsgebieden en oeverzones.

* [OGC API Features Overstromingsgebieden en oeverzones - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/OGOZ/ogc/features/v1): Directe downloadservice voor de Overstromingsgebieden en oeverzones.

* [OGC API Features Parameters woningkwaliteitsbeleid - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Woningkwaliteitbeleid/ogc/features): Directe downloadservice voor de parameters woningkwaliteitsbeleid lokale besturen.

* [OGC API Features Parameters woningkwaliteitsbeleid - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Woningkwaliteitbeleid/ogc/features/v1): Directe downloadservice voor de parameters woningkwaliteitsbeleid lokale besturen.

* [OGC API Features Percelen Vlaamse en Lokale Overheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/PercelenVLO/ogc/features): Directe downloadservice voor de percelen in eigendom van en/of in gebruik door de Vlaamse en Lokale Overheden.

* [OGC API Features Percelen Vlaamse en Lokale Overheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/PercelenVLO/ogc/features/v1): Directe downloadservice voor de percelen in eigendom van en/of in gebruik door de Vlaamse en Lokale Overheden.

* [OGC API Features Potentieel natuurlijke vegetatie - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/PNV/ogc/features): Directe downloadservice voor Potentieel natuurlijke vegetatie.

* [OGC API Features Potentieel natuurlijke vegetatie - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/PNV/ogc/features/v1): Directe downloadservice voor Potentieel natuurlijke vegetatie.

* [OGC API Features Recent overstroomde gebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ROG/ogc/features): Directe downloadservice voor de Recent overstroomde gebieden.

* [OGC API Features Recent overstroomde gebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/ROG/ogc/features/v1): Directe downloadservice voor de Recent overstroomde gebieden.

* [OGC API Features Reiswegen De Lijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Reiswegen/ogc/features): Directe downloadservice voor de reiswegen van de Vlaamse vervoersmaatschappij De Lijn.

* [OGC API Features Reiswegen De Lijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Reiswegen/ogc/features/v1): Directe downloadservice voor de reiswegen van de Vlaamse vervoersmaatschappij De Lijn.

* [OGC API Features Rioolinventaris Vlaanderen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Rioolinventaris/ogc/features): Directe downloadservice voor de Rioolinventaris Vlaanderen.

* [OGC API Features Rioolinventaris Vlaanderen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Rioolinventaris/ogc/features/v1): Directe downloadservice voor de Rioolinventaris Vlaanderen.

* [OGC API Features Rioolwaterzuiveringsinstallatie Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-UWWTP/ogc/features): Directe downloadservice voor de Rioolwaterzuiveringsinstallaties Richtlijn Stedelijk afvalwater.

* [OGC API Features Rioolwaterzuiveringsinstallatie Richtlijn Stedelijk afvalwater - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Uwwtd-UWWTP/ogc/features/v1): Directe downloadservice voor de Rioolwaterzuiveringsinstallaties Richtlijn Stedelijk afvalwater.

* [OGC API Features Ruilverkaveling van landeigendommen in der minne - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkInDerMinne/ogc/features): Directe downloadservice voor de Ruilverkaveling van landeigendommen in der minne.

* [OGC API Features Ruilverkaveling van landeigendommen in der minne - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkInDerMinne/ogc/features/v1): Directe downloadservice voor de Ruilverkaveling van landeigendommen in der minne.

* [OGC API Features Ruilverkaveling van landeigendommen uit kracht van wet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkKrachtWet/ogc/features): Directe downloadservice voor de Ruilverkaveling van landeigendommen uit kracht van wet.

* [OGC API Features Ruilverkaveling van landeigendommen uit kracht van wet - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkKrachtWet/ogc/features/v1): Directe downloadservice voor de Ruilverkaveling van landeigendommen uit kracht van wet.

* [OGC API Features Ruilverkaveling van landeigendommen uit kracht van wet bij uitvoering van grote infrastructuurwerken - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkGrInfraWerken/ogc/features): Directe downloadservice voor de Ruilverkaveling van landeigendommen uit kracht van wet bij uitvoering van grote infrastructuurwerken.

* [OGC API Features Ruilverkaveling van landeigendommen uit kracht van wet bij uitvoering van grote infrastructuurwerken - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RvkGrInfraWerken/ogc/features/v1): Directe downloadservice voor de Ruilverkaveling van landeigendommen uit kracht van wet bij uitvoering van grote infrastructuurwerken.

* [OGC API Features RVV-Themabestand - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/RVVThemabestand/ogc/features/v1): Directe overdrachtdienst voor het RVV-Themabestand.

* [OGC API Features Statistische sectoren van België - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/StatistischeSectoren/ogc/features): Directe downloadservice met de data van de Statistische sectoren van België, zoals die zijn vastgesteld door Statbel - Statistics Belgium.

* [OGC API Features Statistische sectoren van België - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/StatistischeSectoren/ogc/features/v1): Directe downloadservice met de data van de Statistische sectoren van België, zoals die zijn vastgesteld door Statbel - Statistics Belgium.

* [OGC API Features Steunzones - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Steunzones/ogc/features): Directe downloadservice voor de Steunzones.

* [OGC API Features Steunzones - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Steunzones/ogc/features/v1): Directe downloadservice voor de Steunzones.

* [OGC API Features Stroomgebiedsdistricten Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-RiverBasinDistrict/ogc/features): Directe downloadservice voor de Stroomgebiedsdistricten in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Stroomgebiedsdistricten Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-RiverBasinDistrict/ogc/features/v1): Directe downloadservice voor de Stroomgebiedsdistricten in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Sub-unit Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SubUnit/ogc/features): Directe downloadservice voor de Sub-units in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Sub-unit Kaderrichtlijn water - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WFD-SubUnit/ogc/features/v1): Directe downloadservice voor de Sub-units in uitvoering van 2000/60/EG (Kaderrichtlijn Water) zoals gerapporteerd aan de Europese Commissie.

* [OGC API Features Traditionele landschappen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/TradLa/ogc/features): Directe downloadservice voor de traditionele landschappen.

* [OGC API Features Traditionele landschappen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/TradLa/ogc/features/v1): Directe downloadservice voor de traditionele landschappen.

* [OGC API Features Van nature overstroombare gebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NOG/ogc/features): Directe downloadservice voor de van nature overstoombare gebieden.

* [OGC API Features Van nature overstroombare gebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/NOG/ogc/features/v1): Directe downloadservice voor de van nature overstoombare gebieden.

* [OGC API Features Verrijkte KruispuntBank Ondernemingen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VKBO/ogc/features): Directe downloadservice voor de Verrijkte KruispuntBank Ondernemingen.

* [OGC API Features Verrijkte KruispuntBank Ondernemingen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VKBO/ogc/features/v1): Directe downloadservice voor de Verrijkte KruispuntBank Ondernemingen.

* [OGC API Features Vervoersnetwerken weg - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/tn-ro/ogc/features): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Vervoersnetwerken - weg (geharmoniseerde data).

* [OGC API Features Vervoersnetwerken weg - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/tn-ro/ogc/features/v1): INSPIRE-compatibele directe overdrachtdienst met objecttypes uit INSPIRE Annex I Thema Vervoersnetwerken - weg (geharmoniseerde data).

* [OGC API Features Vlaamse Hydrografische Atlas - Waterlopen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VHAWaterlopen/ogc/features): Directe downloadservice voor de Vlaamse Hydrografische Atlas - Waterlopen.

* [OGC API Features Vlaamse Hydrografische Atlas - Waterlopen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VHAWaterlopen/ogc/features/v1): Directe downloadservice voor de Vlaamse Hydrografische Atlas - Waterlopen.

* [OGC API Features Vlaamse Leveringsgebieden Drinkwater drinkwaterrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/DWD-WaterSupplyZones/ogc/features): Directe downloadservice voor de Vlaamse Leveringsgebieden Drinkwater zoals samengesteld voor rapportering onder de drinkwaterrichtlijn aan de Europese Commissie.

* [OGC API Features Vlaamse Leveringsgebieden Drinkwater drinkwaterrichtlijn - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/DWD-WaterSupplyZones/ogc/features/v1): Directe downloadservice voor de Vlaamse Leveringsgebieden Drinkwater zoals samengesteld voor rapportering onder de drinkwaterrichtlijn aan de Europese Commissie.

* [OGC API Features Vlaamse Zwemwaterlocaties - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VlaamseZwemwaterlocaties/ogc/features): Directe downloadservice voor de Vlaamse Zwemwaterlocaties.

* [OGC API Features Vlaamse Zwemwaterlocaties - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VlaamseZwemwaterlocaties/ogc/features/v1): Directe downloadservice voor de Vlaamse Zwemwaterlocaties.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG/ogc/features): Directe downloadservice voor het Voorlopig referentiebestand gemeentegrenzen.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG/ogc/features/v1): Directe downloadservice voor het Voorlopig referentiebestand gemeentegrenzen.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen 2003 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG2003/ogc/features): Directe overdrachtdienst voor het Voorlopig referentiebestand gemeentegrenzen 2003.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen 2003 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG2003/ogc/features/v1): Directe overdrachtdienst voor het Voorlopig referentiebestand gemeentegrenzen 2003.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen 2019 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG2019/ogc/features): Directe overdrachtdienst voor het Voorlopig referentiebestand gemeentegrenzen 2019.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen 2019 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG2019/ogc/features/v1): Directe overdrachtdienst voor het Voorlopig referentiebestand gemeentegrenzen 2019.

* [OGC API Features Voorlopig referentiebestand gemeentegrenzen 2025 - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/VRBG2025/ogc/features/v1): Directe overdrachtdienst voor het Voorlopig referentiebestand gemeentegrenzen 2025.

* [OGC API Features Waterkwaliteitsdoelstellingen Wateroppervlakken - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WokwdoelWateropp/ogc/features): Directe downloadservice voor de Waterkwaliteitsdoelstellingen wateroppervlakken.

* [OGC API Features Waterkwaliteitsdoelstellingen Wateroppervlakken - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WokwdoelWateropp/ogc/features/v1): Directe downloadservice voor de Waterkwaliteitsdoelstellingen wateroppervlakken.

* [OGC API Features Watersystemen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Watersystemen/ogc/features): Directe downloadservice voor de geografische indeling van watersystemen.

* [OGC API Features Watersystemen - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Watersystemen/ogc/features/v1): Directe downloadservice voor de geografische indeling van watersystemen.

* [OGC API Features Wegenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wegenregister/ogc/features): De OGC API Features van het Wegenregister laat toe om objecten uit het Wegenregister geometrisch te bevragen. Het Wegenregister is het middenschalig referentiebestand van de wegen in Vlaanderen. Het bevat alle wegen van Vlaanderen, met bijhorende attribuutgegevens.

* [OGC API Features Wegenregister - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wegenregister/ogc/features/v1): De OGC API Features van het Wegenregister laat toe om objecten uit het Wegenregister geometrisch te bevragen. Het Wegenregister is het middenschalig referentiebestand van de wegen in Vlaanderen. Het bevat alle wegen van Vlaanderen, met bijhorende attribuutgegevens.

* [OGC API Features Werkingsgebieden van wildbeheereenheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wbe/ogc/features): Directe downloadservice voor de werkingsgebieden van wildbeheereenheden.

* [OGC API Features Werkingsgebieden van wildbeheereenheden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/Wbe/ogc/features/v1): Directe downloadservice voor de werkingsgebieden van wildbeheereenheden.

* [OGC API Features Woningbouw- en woonvernieuwingsgebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WoningbWoonvernieuwing/ogc/features): Directe downloadservice voor de woningbouw- en woonvernieuwingsgebieden.

* [OGC API Features Woningbouw- en woonvernieuwingsgebieden - geo.api.vlaanderen.be](https://geo.api.vlaanderen.be/WoningbWoonvernieuwing/ogc/features/v1): Directe downloadservice voor de woningbouw- en woonvernieuwingsgebieden.

